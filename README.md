# Electronアプリケーションテンプレート

Electronアプリケーションを作る際のテンプレート。Electronで何かしてみたい時にはここから始めればよい。

## clone

```
git clone https://taq@bitbucket.org/taq/electron-template.git
```

## 登録されているパッケージ

* electron-packager ^7.0.1
* electron-prebuilt ^0.37.8

以下コマンドにて依存パッケージをインストールする。
```
npm install
```

## デバッグ

```
npm run debug
```

## ビルド

[https://github.com/electron-userland/electron-packager](https://github.com/electron-userland/electron-packager)